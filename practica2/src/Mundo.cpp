// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "Mundo.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{

}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i,j;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();
	
	for(i=0; i<nEsferas; i++)
	{
		esferas[i]->Dibuja();
	}
	
	if(disparo1A == true) disparo1->Dibuja();
	if(disparo2A == true) disparo2->Dibuja();
	

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{
		
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	
	jugador1.SetCentro();
	jugador2.SetCentro();
	
	esfera.Mueve(0.025f);
	
	int i;
	int j;
	
	for(i=0; i<nEsferas; i++)
	{
		esferas[i]->Mueve(0.025f);
	}
	
	if(disparo1A == true) disparo1->Mueve(0.025f);
	if(disparo2A == true) disparo2->Mueve(0.025f);
	
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}
	
	for(i=0;i<paredes.size();i++)
	{
		for(j=0; j<nEsferas; j++)
		{
			paredes[i].Rebota(*(esferas[j]));
		}
	}
	
	if(disparo1A == true)
	{
		for(i=0;i<paredes.size();i++)
		{
			paredes[i].Rebota(*(disparo1));
		}
	}
	
	if(disparo2A == true)
	{
		for(i=0;i<paredes.size();i++)
		{
			paredes[i].Rebota(*(disparo2));
		}
	}

//	jugador1.Rebota(esfera);
//	jugador2.Rebota(esfera);

	if(jugador1.Rebota(esfera))nRebotes++;
	if(jugador2.Rebota(esfera))nRebotes++;
	
	for(i=0; i<nEsferas; i++)
	{
		if(jugador1.Rebota(*(esferas[i])))nRebotes++;
		if(jugador2.Rebota(*(esferas[i])))nRebotes++;
	}


	if(fondo_izq.Rebota(esfera))
	{
		DestruyeEsferas();
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
	}

	if(fondo_dcho.Rebota(esfera))
	{
		DestruyeEsferas();
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
	}
	
	for(i=0; i<nEsferas; i++)
	{
		if(fondo_izq.Rebota(*(esferas[i])))
		{
			DestruyeEsferas();
			esfera.centro.x=0;
			esfera.centro.y=rand()/(float)RAND_MAX;
			esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
			esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
			puntos2++;
		}

		if(fondo_dcho.Rebota(*(esferas[i])))
		{
			DestruyeEsferas();
			esfera.centro.x=0;
			esfera.centro.y=rand()/(float)RAND_MAX;
			esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
			esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
			puntos1++;
		}	
	}
	
	if(disparo1A == true)
	{
		if(fondo_dcho.Rebota(*(disparo1)))
		{
			delete disparo1;
			disparo1A = false;
			puntos1++;
		}
		
		if(jugador2.Rebota(*(disparo1)))
		{
			delete disparo1;
			disparo1A = false;
			puntos2++;
		}
	}
	
	if(disparo2A == true)
	{
		if(fondo_izq.Rebota(*(disparo2)))
		{
			delete disparo2;
			disparo2A = false;
			puntos2++;
		}
		
		if(jugador1.Rebota(*(disparo2)))
		{
			delete disparo2;
			disparo2A = false;
			puntos1++;
		}
	}
	
	
	if(nRebotes >= 3) 
	{
		CreaEsferas();
		nRebotes = 0;
	}
	
	for(i=0; i<nEsferas; i++)
	{
		esfera.Rebota(esferas[i]);
	}
	
	for(i=0; i<nEsferas; i++)
	{
		for(j=i+1; j<nEsferas; j++)
		{
			esferas[i]->Rebota(esferas[j]);
		}
	}
	
	if(disparo1A == true && (esfera.Rebota(disparo1)))
	{
		delete disparo1;
		disparo1A = false;
	}
	if(disparo2A == true && (esfera.Rebota(disparo2)))
	{
		delete disparo2;
		disparo2A = false;
	}

	for(i=0; i<nEsferas; i++)
	{
		if(disparo1A == true && (esferas[i]->Rebota(disparo1)))
		{
			delete disparo1;
			disparo1A = false;
		}
		if(disparo2A == true && (esferas[i]->Rebota(disparo2)))
		{
			delete disparo2;
			disparo2A = false;
		}
	}

}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
	case 'q':jugador1.DisminuyeVelocidad(0);break;
	case 'e':jugador1.AumentaVelocidad(0);break;
	case 's':jugador1.DisminuyeVelocidad(1);break;
	case 'w':jugador1.AumentaVelocidad(1);break;
	case 'l':jugador2.DisminuyeVelocidad(1);break;
	case 'o':jugador2.AumentaVelocidad(1);break;
	case 'i':jugador2.DisminuyeVelocidad(0);break;
	case 'p':jugador2.AumentaVelocidad(1);break;
	case 'd':CreaDisparo(jugador1.centro, 4.0f, 1);break;
	case 'k':CreaDisparo(jugador2.centro, -4.0f, 2);break;
	default:break;

	}
}

void CMundo::Init()
{
	nRebotes = 0;
	nEsferas = 0;
	
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}

void CMundo::CreaEsferas(void)
{
	if(nEsferas <= MAX_ESFERAS)
	{
		esferas[nEsferas] = new Esfera(20); //El numero da igual, solo es para diferenciar los constructores
		nEsferas++;
	}
}

void CMundo::DestruyeEsferas(void)
{
	int i;
	for(i=0; i<nEsferas; i++)
	{
		delete esferas[i];
	}
	nEsferas = 0;
}

void CMundo::CreaDisparo(Vector2D centro, float velocidadX, int jugador)
{
	switch(jugador)
	{
		case 1: if(disparo1A == false)
				{
					disparo1 = new Esfera(centro, velocidadX);
					disparo1A = true;
				}
				break;
		case 2: if(disparo2A == false)
				{
					disparo2 = new Esfera(centro, velocidadX);
					disparo2A = true;
				}
				break;
		default:break;	
	}
}
